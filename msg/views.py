from django.shortcuts import render, redirect
from .models import Message
from . import forms

def message_list(request):
    messages = Message.objects.all()
    message_form = forms.CreateMessageForm()
    response = {
        'msgs' : messages,
        'form' : message_form
    }
    return render(request, 'msg/message_list.html', response)

def message_create(request):
    message_form = forms.CreateMessageForm(request.POST)
    if request.method == "POST" and message_form.is_valid():
        Message.objects.create(
            submiter = message_form.cleaned_data['submiter'],
            message = message_form.cleaned_data['message']
        )
    return redirect('msg:message_list')