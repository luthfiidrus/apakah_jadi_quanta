from django.db import models

class Message(models.Model):
    submiter = models.CharField(max_length=50)
    message = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return "{} - {}".format(self.submiter, self.snipped_message())
    
    def snipped_message(self):
        if len(self.message) > 20 :
            return self.message[:20] + " ..."
        return self.message